+++
title = "#WhoAmI"
+++

Bonjour 👋

Je m'appelle Cyril, j'accompagne les entreprises d'IoT/embarqué dans la mise en place sereine de technologies modernes et rentables (Rust, Git...).

Durant mon expérience de 8+ années en développement embarqué industriel (Airbus, Intel, Alstom, ABB...), j'ai très souvent observé que la discordance (voire l'obsolescence) des technologies, outils et pratiques nuit à la vitesse de développement, à la pérennité des recrutements et complique fortement la maintenance.

Au-delà d'une prestation de développement, mon expérience de formateur et mon appétence pour la découverte et la pédagogie vous permettront de prendre solidement pied dans un domaine plébiscité en pleine explosion et de profiter de mon savoir-faire même après mon départ.

J'ai à cœur d'aider vos développeurs à prendre plaisir plutôt qu'à s'arracher les cheveux, vos recruteurs à proposer des opportunités enthousiasmantes et prometteuses ainsi que de vous permettre d'étoffer votre vitrine avec des projets ambitieux, innovants et élégants.

Si vous souhaitez prendre le virage, n'hésitez plus, contactez-moi 🚀