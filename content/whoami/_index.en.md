+++
title = "#WhoAmI"
+++

Hello 👋

My name is Cyril, and I assist IoT/embedded systems companies in securing their adoption of modern and profitable technologies such as Rust and Git.

During my 8+ years of experience in industrial embedded development (at Airbus, Intel, Alstom, ABB...), I have very often observed that the discordance (or even obsolescence) of technologies, tools and practices hinders development speed, affects recruitment sustainability, and significantly complicates maintenance.

Beyond a development service, my experience as a trainer and my passion for discovery and teaching will enable you to firmly establish yourself in an exploding, highly demanded field, and to benefit from my expertise even after my departure.

I am committed to helping your developers enjoy their work rather than pulling their hair out, your recruiters offer exciting and promising opportunities, and to allow you to showcase ambitious, innovative, and elegant projects.

If you want to take this turn, don't hesitate any longer, contact me 🚀