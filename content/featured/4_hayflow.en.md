+++
title = "HayFlow Zola Template"

[extra]
link = "https://gitlab.com/cyril-marpaud/hayflow"
+++

This website was created for fun using [Zola](https://www.getzola.org), a static site generator written in Rust. [The template](https://gitlab.com/cyril-marpaud/hayflow) is completely modular and available for free under the **CC-BY-SA 4.0** license.