+++
title = "I2C LCD"

[extra]
link = "https://gitlab.com/cyril-marpaud/i2c_lcd"
+++

Driver pour contrôleur HD44780 via bus I2C écrit en Rust et permettant de piloter un écran LCD avec exemple concret sur [BBC micro:bit](https://microbit.org) (mesure et affichage de température via le capteur embarqué)