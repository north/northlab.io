+++
title = "HayFlow Zola Template"

[extra]
link = "https://gitlab.com/cyril-marpaud/hayflow"
+++

Ce site web a été réalisé pour le fun avec [Zola](https://www.getzola.org), un générateur de site statique écrit en Rust. [Le template](https://gitlab.com/cyril-marpaud/hayflow) est complètement modulaire et disponible gratuitement sous license **CC-BY-SA 4.0**.