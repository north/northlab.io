+++
title = "Git"

[extra]
icons = ["fa-brands fa-git-alt"]
+++

Why use it? Productivity, collaboration, simplicity... or simply because it's the de facto standard of the open source community!