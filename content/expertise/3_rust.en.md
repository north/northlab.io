+++
title = "Rust"

[extra]
icons = ["fa-brands fa-rust"]
+++

Performance and modernity finally accessible in an elegant and highly praised language!
