+++
title = "Git"

[extra]
icons = ["fa-brands fa-git-alt"]
+++

Pourquoi l'utiliser ? Productivité, collaboration, simplicité... Ou simplement parce que c'est le standard de facto de la communauté du libre !