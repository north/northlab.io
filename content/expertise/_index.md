+++
title = "Expertise"
sort_by = "slug"

[extra]
card_type = "columns"
+++

Diplômé de la filière **Électronique et Physique Appliquée** en majeure **Signal, Automatique et Télécommunications pour l'Embarqué** à l'ENSICAEN en 2013, j'ai depuis travaillé avec des entreprises de différentes tailles, parfois à la renommée mondiale (*Intel*, *Airbus*, *Alstom*, *ABB*) et dans de nombreux secteurs (*Télécoms*, *Énergie*, *Aéronautique*, *Ferroviaire*, *Cybersécurité*...).