+++
title = "Backend Linux"

[extra]
icons = ["fa-brands fa-linux"]
+++

Mon champ de compétences couvre le développement sous Linux, qu'il soit applicatif ou embarqué.